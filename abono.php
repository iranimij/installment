<?php

/**
 * Plugin Name:       ABONO
 * Plugin URI:        http://www.hamyarwoo.com
 * Description:       ABONO Installment wordpress plugin
 * Version:           1.0.0
 * Author:            Iman Heydari
 * Author URI:        http://www.iranimij.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * GitLab Plugin URI: https://gitlab.com/iranimij/installment
 * Text Domain:       abono
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'ABONO_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-abono-activator.php
 */
function activate_abono() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-abono-activator.php';
	Abono_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-abono-deactivator.php
 */
function deactivate_abono() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-abono-deactivator.php';
	Abono_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_abono' );
register_deactivation_hook( __FILE__, 'deactivate_abono' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-abono.php';
define('AB_DIR', plugin_dir_path(__FILE__));
define('AB_ADMIN_DIR', plugin_dir_path(__FILE__).'admin/');
define('AB_PUBLIC_DIR', plugin_dir_path(__FILE__).'public/');
define('AB_URL', plugin_dir_url(__FILE__));
define('AB_ADMIN_URL', plugin_dir_url(__FILE__).'admin/');
define('AB_PUBLIC_URL', plugin_dir_url(__FILE__).'public/');





add_action('init', 'myStartSession', 1);
add_action('wp_logout', 'myEndSession');
add_action('wp_login', 'myEndSession');

function myStartSession()
{
    if (!session_id()) {
        session_start();
    }
}

function myEndSession()
{
    session_destroy();
}
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_abono() {

	$plugin = new Abono();
	$plugin->run();

}

define("HAMYARWOO_LICENSE","hamyarwoo_license");
define("HAMYARWOO","hamyarwoo");
add_action('admin_menu','abono_add_license_menu');

function abono_add_license_menu(){

    if (empty(get_option("hw_license"))){
    add_menu_page(
            __('لایسنس آبونو', 'abono'),
            __('لایسنس آبونو', 'abono'),
            'manage_options',
            'license',
            "license_template_func",
            'dashicons-editor-table',
            90);
    }
}
define("RTL_API","rtlcb784a3dcaf5db1b83a8d9334c6cb3");
function license_template_func(){
    if (isset($_POST['username'])){
        $result = sendToRtl(RTL_API,$_POST['username'],$_POST['order_id'],site_url());
        switch ($result) {
            case '1':
                $error = NULL;
                break;
            case '-1':
                $error = ' کد API اشتباه است.';
                break;
            case '-2':
                $error = ' نام کاربری اشتباه است.';
                break;
            case '-3':
                $error = ' کد سفارش اشتباه است.';
                break;
            case '-4':
                $error = ' کد سفارش  قبل ثبت شده است .';
                break;
            case '-5':
                $error = ' کد سفارش مربوط به این نام کاربری نمیباشد.';
                break;
            default:
                $error = ' خطای نا مشخص';
                break;
        }
        if ($result == 1){
            $encrypted_string = password_hash(HAMYARWOO_LICENSE, PASSWORD_DEFAULT);
            update_option("hw_license",$encrypted_string);
            wp_redirect(admin_url("admin.php?page=abono"));
            die();
        }
    }
    if ($error){
        ?>
        <div class="error notice">
            <p><?php _e( $error, 'my_plugin_textdomain' ); ?></p>
        </div>
        <?PHP
    }
?>

    <form action="" method="post">
        <table class="form-table" role="presentation">
            <tbody>
            <tr class="user-user-login-wrap">
                <th>
                    <label for="user_login"><?= _e("نام کاربری") ?></label>
                </th>
                <td>
                    <input type="text" name="username" value="">
                    <p class="description"><?= __("نام کاربری راست چین .", "abono"); ?></p>
                </td>
            </tr>
            <tr class="user-user-login-wrap">
                <th>
                    <label for="user_login"><?= _e("شماره سفارش") ?></label>
                </th>
                <td>
                    <input type="text" name="order_id" value="">
                    <p class="description"><?= __("شماره سفاش .", "abono"); ?></p>
                </td>
            </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="ذخیره تغییرات"></p>
    </form>
<?PHP

}





$hw_license = get_option("hw_license");
if (!empty($hw_license)){
    $decrypted_string=openssl_decrypt($hw_license,"AES-128-ECB",HAMYARWOO);
    if (password_verify(HAMYARWOO_LICENSE,$hw_license)){
            run_abono();
    }else{
        update_option("hw_license",null);

    }
}

function sendToRtl($api, $username, $order_id, $domain)
{
    $url = 'http://www.rtl-theme.com/oauth/';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "api=$api&username=$username&order_id=$order_id&domain=$domain");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $res = curl_exec($ch);
    curl_close($ch);
    return $res;
}
