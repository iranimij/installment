<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.iranimij.com
 * @since      1.0.0
 *
 * @package    Abono
 * @subpackage Abono/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Abono
 * @subpackage Abono/includes
 * @author     Iman Heydari <iranimij@gmail.com>
 */
class Abono {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Abono_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'ABONO_VERSION' ) ) {
			$this->version = ABONO_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'abono';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Abono_Loader. Orchestrates the hooks of the plugin.
	 * - Abono_i18n. Defines internationalization functionality.
	 * - Abono_Admin. Defines all hooks for the admin area.
	 * - Abono_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/helpers/jdf.php';
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-abono-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-abono-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-abono-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-abono-public.php';

		$this->loader = new Abono_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Abono_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Abono_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Abono_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        $this->loader->add_action('admin_menu',$plugin_admin,'abono_add_admin_menu');
        $this->loader->add_action('add_meta_boxes',$plugin_admin,'wpse_add_custom_meta_box_2');
        $this->loader->add_action('save_post',$plugin_admin,'wpse_save_meta_fields');
        $this->loader->add_action('new_to_publish',$plugin_admin,'wpse_save_meta_fields');
        $this->loader->add_action('template_include',$plugin_admin,'installment_template_redirect');
        $this->loader->add_action('wc_order_statuses',$plugin_admin,'add_installment_abono_status');
        $this->loader->add_action('woocommerce_admin_order_data_after_billing_address',$plugin_admin,'misha_editable_order_meta_shipping');
        $this->loader->add_action('woocommerce_process_shop_order_meta',$plugin_admin,'misha_save_shipping_details');
        $this->loader->add_action('msg_email_send_installment',$plugin_admin,'msg_email_send_installment_func');
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Abono_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'woocommerce_after_add_to_cart_button', $plugin_public, 'after_add_to_card_func' );
		$this->loader->add_action( 'woocommerce_before_add_to_cart_button', $plugin_public, 'before_add_to_card_func' );
		$this->loader->add_action( 'wp_ajax_get_calculated_installment_info', $plugin_public, 'get_calculated_installment_info' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_calculated_installment_info', $plugin_public, 'get_calculated_installment_info' );
		$this->loader->add_action( 'woocommerce_proceed_to_checkout', $plugin_public, 'woocommerce_cross_sell_display1' );
		$this->loader->add_action( 'woocommerce_checkout_process', $plugin_public, 'validate_new_installment_field' );
		$this->loader->add_action( 'woocommerce_review_order_before_order_total', $plugin_public, 'cart_totals_after_order_total' );
		$this->loader->add_action( 'woocommerce_calculated_total', $plugin_public, 'abono_calculated_total' ,2,2);
		$this->loader->add_action( 'woocommerce_checkout_update_order_meta', $plugin_public, 'customise_checkout_field_update_order_meta' ,2,1);
		$this->loader->add_action( 'woocommerce_admin_order_totals_after_shipping', $plugin_public, 'order_item_after_shipping' ,2,1);
		$this->loader->add_action( 'woocommerce_thankyou', $plugin_public, 'change_status_in_thankyou_page' ,2,1);
		$this->loader->add_action( 'init', $plugin_public, 'add_new_statuses' ,2);
		$this->loader->add_action( 'woocommerce_get_order_item_totals', $plugin_public, 'show_order_installment_info' ,2,2);
		$this->loader->add_action( 'woocommerce_after_checkout_billing_form', $plugin_public, 'customise_checkout_field' ,2,1);

	    }

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Abono_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
