<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.iranimij.com
 * @since      1.0.0
 *
 * @package    Abono
 * @subpackage Abono/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Abono
 * @subpackage Abono/includes
 * @author     Iman Heydari <iranimij@gmail.com>
 */
class Abono_Activator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        if (!wp_next_scheduled('msg_email_send_installment')) {

            wp_schedule_event(strtotime(date('Y-m-d 12:00:00')), 'daily', 'msg_email_send_installment');

        }

        update_option("abono_alert_message", "{fname} {lname} تا تاریخ چک بعدی شما 3 روز مانده است .");
        update_option("abono_prepayment_percent", explode(PHP_EOL, str_replace(" ", "", "10
20
30
40")));
        update_option("abono_month_number", explode(PHP_EOL, str_replace(" ", "", "1
2
3
4
5")));
        update_option("abono_check_number", explode(PHP_EOL, str_replace(" ", "", "1
2
3
4")));
        update_option("abono_profit_percent", intval(10));
        update_option("abono_sms_send_number", "50002060406090");
    }

}
