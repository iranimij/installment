<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.iranimij.com
 * @since      1.0.0
 *
 * @package    Abono
 * @subpackage Abono/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Abono
 * @subpackage Abono/includes
 * @author     Iman Heydari <iranimij@gmail.com>
 */
class Abono_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
