<?PHP
if (isset($_POST['pre_pay'])) {
    ob_clean();
    ob_end_flush();
    $_SESSION['abono_installment'] = [
        "variation_id" => !empty($_POST['variation_id']) ? $_POST['variation_id'] : null,
        "product_id" => !empty($_POST['product_id']) ? $_POST['product_id'] : 0,
        "quantity" => !empty($_POST['quantity']) ? $_POST['quantity'] : 0,
        "pre_pay" => !empty($_POST['pre_pay']) ? $_POST['pre_pay'] : 0,
        "installment_num" => !empty($_POST['installment_num']) ? $_POST['installment_num'] : 0,
        "cheque_number" => !empty($_POST['cheque_number']) ? $_POST['cheque_number'] : 0,
    ];
    ob_start();

    wp_redirect(esc_url(wc_get_checkout_url()));


}


//$product_id = get_the_ID();

foreach (WC()->cart->get_cart() as $cart_item) {
    $product = $cart_item['data'];
    $quantity = $cart_item['quantity'];
    $variation_id = !empty($cart_item['variation_id']) ? $cart_item['variation_id'] : null;
    if (!empty($product)) {
        // $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product->ID ), 'single-post-thumbnail' );
        @$ids = $product->id;
        @$ids1[] = $product->id;
        $installment[] = get_post_meta($ids, 'installment', true);
        // to display only the first product image uncomment the line bellow
        // break;
    }
}
//var_dump(sizeof($ids1));

global $woocommerce;
$cart_url = wc_get_cart_url();

if (sizeof($ids1) > 1) {
    echo "<span style='width: 100%;display: inline-block;text-align: center;border: 1px solid red;padding: 10px;color: red;font-size: 15pt;'>در هر خرید قسطی فقط باید یک نوع محصول خریداری کنید</span>
<br>

<a style='margin-top: 20px;padding: 7px 10px;background: plum;border-radius: 4px;box-shadow: 2px 3px 4px #ccc;display: inline-block' href='" . $cart_url . "'>باز گشت به سبد خرید</a>


";
    echo '<style>

.property{display: none}

</style>';
    die();
}


get_header();
$product_id = $ids;
$pish_darsad_pro = get_post_meta($product_id, 'pish_darsad_pro', true);
$pish_check_pro = get_post_meta($product_id, 'pish_check_pro', true);
$pish_month_pro = get_post_meta($product_id, 'pish_month_pro', true);
$installment_is_active = get_post_meta($product_id, 'installment', true);

if ($pish_darsad_pro != "") {

    $pish_darsad = $pish_darsad_pro;

} else {
    $pish_darsad = get_option('abono_prepayment_percent');
}

if ($pish_check_pro != "") {
    $pish_check = $pish_check_pro;
} else {
    $pish_check = get_option('abono_check_number');
}

if ($pish_month_pro != "") {
    $pish_month = $pish_month_pro;
} else {
    $pish_month = get_option('abono_month_number');
}
if ($installment_is_active):
    ?>
    <style>
        .abono_continiue_installment {
            display: none;
        }

        .abono_continiue_installment.active {
            display: block;
        }

        .installment_field_box {
            padding: 10px;
            display: flex;
            align-items: baseline;
        }

        .installment_field_box label {
            width: 150px;
            display: inline-block;
            font-weight: bold;
        }

        .installment_field_box select {
            width: 150px;
        }
        .abono_card_template{
            width: 300px;
            height: 400px;
            margin: 10px auto 30px auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 10px;
            box-shadow: 0px 0px 6px #ccc;
        }
        @media (min-width: 700px) {
            .abono_card_template{
                width: 600px;
                height: auto;
                margin: 10px auto 30px auto;
                padding: 20px;
                border: 1px solid #ccc;
                border-radius: 10px;
                box-shadow: 0px 0px 6px #ccc;
            }
        }
        .installment_field_box select{
            display: inline-block;
        }
    </style>

    <div class="abono_card_template">
    <h2> قیمت کل خرید نقدی :
        <span><?= $woocommerce->cart->get_cart_total(); ?></span>
    </h2>

    <form action="" method="post">
    <?PHP
    if ($variation_id) {
        ?>
        <input type="hidden" name="variation_id" value="<?= $variation_id ?>" class="variation_id">
    <?PHP } ?>
    <input type="hidden" name="quantity" value="<?= $quantity ?>">
    <input type="hidden" name="product_id" value="<?= $ids ?>">
    <button name="add-to-cart" value="<?= $ids ?>" style="display: none"></button>
    <div class="installment_parent">

        <div class="installment_field_box">
            <label for="">پیش پرداخت</label>
            <select name="pre_pay" id="pre_pay">
                <?PHP
                if (is_array($pish_darsad) && sizeof($pish_darsad) > 0) {
                    ?>
                    <option value="">انتخاب کنید</option>
                    <?PHP
                    foreach ($pish_darsad as $item) {
                        ?>
                        <option value="<?= $item ?>"><?= $item ?> درصد</option>
                        <?PHP

                    }
                }
                ?>
            </select>
        </div>

        <div class="installment_field_box">
            <label for="">تعداد اقساط</label>
            <select name="installment_num" id="installment_num">
                <?PHP
                if (is_array($pish_month) && sizeof($pish_month) > 0) {
                    ?>
                    <option value="">انتخاب کنید</option>
                    <?PHP
                    foreach ($pish_month as $item) {
                        ?>
                        <option value="<?= $item ?>"><?= $item ?> ماهه</option>
                        <?PHP

                    }
                }
                ?>
            </select>
        </div>

        <div class="installment_field_box">
            <label for="">تعداد چک</label>
            <select name="cheque_number" id="cheque_number">
                <?PHP
                if (is_array($pish_check) && sizeof($pish_check) > 0) {
                    ?>
                    <option value="">انتخاب کنید</option>
                    <?PHP
                    foreach ($pish_check as $item) {
                        ?>
                        <option value="<?= $item ?>"> هر <?= $item ?> ماه یک چک</option>
                        <?PHP

                    }
                }
                ?>
            </select>
        </div>

    </div>
    <?PHP
    $installment_is_active = get_post_meta($product_id, 'installment', true);
    if ($installment_is_active):
        ?>
        <span class="single_add_to_cart_button button alt get_installment_info disabled" id="calculate_btn">
محاسبه و نمایش اقساط
        </span>
        <div class="abono_calculated_result installment_parent">

        </div>
        <button type="submit" class="single_add_to_cart_button button alt abono_continiue_installment">ادامه عملیات
        </button>
        </form>
        </div>

    <?PHP endif; ?>
    <?PHP get_footer(); endif; ?>
