<style>
    .btn_style {
        background: #920e61;
        color: #fff;
        padding: 5px 26px;
        margin-right: 20px;
        position: relative;
        bottom: 3px;
    }

    /* The container */
    .container_custom_radio {
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 22px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        margin-right: 20px;
    }

    /* Hide the browser's default radio button */
    .container_custom_radio input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom radio button */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 25px;
        width: 25px;
        background-color: #eee;
        border-radius: 50%;
    }

    /* On mouse-over, add a grey background color */
    .container_custom_radio:hover input ~ .checkmark {
        background-color: #ccc;
    }

    /* When the radio button is checked, add a blue background */
    .container_custom_radio input:checked ~ .checkmark {
        background-color: #2196F3;
    }

    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the indicator (dot/circle) when checked */
    .container_custom_radio input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the indicator (dot/circle) */
    .container_custom_radio .checkmark:after {
        top: 9px;
        left: 9px;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        background: white;
    }
</style>

<?PHP
foreach (WC()->cart->get_cart() as $cart_item) {
    $product = $cart_item['data'];
    if (!empty($product)) {

        @$ids = $product->id;
        $installment[] = get_post_meta($ids, 'installment', true);

    }
}

//var_dump($installment);
if (!in_array(0, $installment)) {
    ?>
    <a href="<?=site_url("installment")?>" class="checkout-button button alt wc-forward">
        خرید اقساطی</a>
<?PHP } ?>