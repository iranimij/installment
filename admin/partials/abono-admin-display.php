<?PHP

!empty($_POST['abono_alert_message']) ? update_option("abono_alert_message",$_POST['abono_alert_message']) : "";

!empty($_POST['abono_prepayment_percent']) ? update_option("abono_prepayment_percent",explode(PHP_EOL, str_replace(" ", "",$_POST['abono_prepayment_percent']))) : "";

!empty($_POST['abono_month_number']) ? update_option("abono_month_number",explode(PHP_EOL, str_replace(" ", "",$_POST['abono_month_number']))) : "";

!empty($_POST['abono_check_number']) ? update_option("abono_check_number",explode(PHP_EOL, str_replace(" ", "",$_POST['abono_check_number']))) : "";

!empty($_POST['abono_profit_percent']) ? update_option("abono_profit_percent",intval($_POST['abono_profit_percent'])) : "";

!empty($_POST['abono_sms_username']) ? update_option("abono_sms_username",$_POST['abono_sms_username']) : "";
!empty($_POST['abono_sms_password']) ? update_option("abono_sms_password",$_POST['abono_sms_password']) : "";
!empty($_POST['abono_sms_send_number']) ? update_option("abono_sms_send_number",$_POST['abono_sms_send_number']) : "";
if (isset($_POST['submit'])){
    ?>
    <div class="notice notice-success is-dismissible">
        <p><?php _e( 'تغییرات با موفقیت ذخیره شد.', 'abono' ); ?></p>
    </div>
<?PHP
}
?>
<div id="wpbody">
    <div id="wpbody-content">
        <div id="profile-page">
            <h1 class="wp-heading-inline"><?PHP _e("تنظیمات  افزونه فروش اقساطی آبونو", "abono"); ?></h1>
            <form action="" method="post">
                <table class="form-table" role="presentation">
                    <tbody>
                    <tr class="user-user-login-wrap">
                        <th>
                            <label for="user_login"><?= _e("متن پیامک هشدار") ?></label>
                        </th>
                        <td>
                            <textarea rows="5" cols="30"
                                      name="abono_alert_message"><?php
                                echo get_option("abono_alert_message");
                                ?></textarea>
                            <p class="description"><?= __("این پیامک سه روز قبل از سر رسید موعد پرداخت چک به کاربر ارسال خواهد شد.{fname} = نام خریدار , {lname} = نام خانوادگی خریدار", "abono") ?></p>
                        </td>
                    </tr>
                    <tr class="user-user-login-wrap">
                        <th>
                            <label for="user_login"><?= _e("درصد های پیشپرداخت") ?></label>
                        </th>
                        <td>
                            <textarea rows="5" cols="30" name="abono_prepayment_percent"><?php
                                $abono_prepayment_percent = get_option("abono_prepayment_percent");
                                $abono_prepayment_percent = implode(PHP_EOL,$abono_prepayment_percent);
                                echo $abono_prepayment_percent; ?></textarea>
                            <p class="description"><?= __("هر مقدار در یک سطر قرار گیرد .", "abono"); ?></p>
                        </td>
                    </tr>
                    <tr class="user-user-login-wrap">
                        <th>
                            <label for="user_login"><?= _e("تعداد ماه های مجاز جهت خرید اقساطی") ?></label>
                        </th>
                        <td>
                            <textarea rows="5" cols="30" name="abono_month_number"><?php
                                $abono_prepayment_percent = get_option("abono_month_number");
                                $abono_prepayment_percent = implode(PHP_EOL,$abono_prepayment_percent);
                                echo $abono_prepayment_percent; ?></textarea>
                            <p class="description"><?= __("هر مقدار در یک سطر قرار گیرد .", "abono"); ?></p>
                        </td>
                    </tr>
                    <tr class="user-user-login-wrap">
                        <th>
                            <label for="user_login"><?= _e("تعداد چک های مجاز در ماه های انتخابی") ?></label>
                        </th>
                        <td>
                            <textarea rows="5" cols="30" name="abono_check_number"><?php
                                $abono_prepayment_percent = get_option("abono_check_number");
                                $abono_prepayment_percent = implode(PHP_EOL,$abono_prepayment_percent);
                                echo $abono_prepayment_percent; ?></textarea>
                            <p class="description"><?= __("هر مقدار در یک سطر قرار گیرد .", "abono"); ?></p>
                        </td>
                    </tr>
                    <tr class="user-user-login-wrap">
                        <th>
                            <label for="user_login"><?= _e("درصد سود جهت فروش اقساطی") ?></label>
                        </th>
                        <td>
                            <input type="text" name="abono_profit_percent" value="<?=!empty(get_option("abono_profit_percent")) ? get_option("abono_profit_percent") : "";?>">
                        </td>
                    </tr>
                    <tr>
                        <th><h2><?= __("مشخصات پنل پیامکی شما", "abono"); ?></h2></th>
                    </tr>
                    <tr class="user-user-login-wrap">
                        <th>
                            <label for="user_login"><?= _e("نام کاربری") ?></label>
                        </th>
                        <td>
                            <input type="text" name="abono_sms_username" value="<?= !empty(get_option("abono_sms_username")) ? get_option("abono_sms_username") : "";?>">
                            <p class="description"><?= __("نام کاربری پنل https://www.payam-resan.com .", "abono"); ?></p>
                        </td>
                    </tr>
                    <tr class="user-user-login-wrap">
                        <th>
                            <label for="user_login"><?= _e("رمز عبور") ?></label>
                        </th>
                        <td>
                            <input type="text" name="abono_sms_password" value="<?= !empty(get_option("abono_sms_password")) ? get_option("abono_sms_password") : ""; ?>">
                            <p class="description"><?= __("رمز عبور پنل https://www.payam-resan.com .", "abono"); ?></p>
                        </td>
                    </tr>
                    <tr class="user-user-login-wrap">
                        <th>
                            <label for="user_login"><?= _e("شماره ارسال کننده پیامک") ?></label>
                        </th>
                        <td>
                            <input type="text" name="abono_sms_send_number" value="<?=!empty(get_option("abono_sms_send_number")) ? get_option("abono_sms_send_number") : "";?>">
                            <p class="description"><?= __("شماره ارسال کننده پنل پیامک https://www.payam-resan.com .", "abono"); ?></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="ذخیره تغییرات"></p>
            </form>
        </div>
    </div>
</div>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
