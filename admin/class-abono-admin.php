<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.iranimij.com
 * @since      1.0.0
 *
 * @package    Abono
 * @subpackage Abono/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Abono
 * @subpackage Abono/admin
 * @author     Iman Heydari <iranimij@gmail.com>
 */
class Abono_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @param string $plugin_name The name of this plugin.
     * @param string $version The version of this plugin.
     * @since    1.0.0
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Abono_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Abono_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style("persian-date-picker", plugin_dir_url(__FILE__) . 'css/persianDatepicker-default.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/abono-admin.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Abono_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Abono_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script("js-date-picker", plugin_dir_url(__FILE__) . 'js/persianDatepicker.min.js', array('jquery'), $this->version, false);
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/abono-admin.js', array('jquery'), $this->version, false);

    }

    public function abono_add_admin_menu()
    {

        add_menu_page(
            __('افزونه فروش اقساطی آبونو', 'abono'),
            __('آبونو', 'abono'),
            'manage_options',
            'abono',
            [$this, "abono_option_page"],
            'dashicons-editor-table',
            90);
    }

    public function abono_option_page()
    {
        require_once AB_ADMIN_DIR . 'partials/abono-admin-display.php';
    }

    public function wpse_add_custom_meta_box_2()
    {
        add_meta_box(
            'custom_meta_box-2',       // $id
            'مشخصات خرید قسطی',                  // $title
            [$this, 'show_custom_meta_box_2'],  // $callback
            'product',                 // $page
            'normal',                  // $context
            'high'                     // $priority
        );
    }

    public function show_custom_meta_box_2()
    {
        global $post;

        // Use nonce for verification to secure data sending
        wp_nonce_field(basename(__FILE__), 'wpse_our_nonce');

        ?>
        <style>
            .inside label {
                width: 30%;
                px;
                display: inline-block
            }

            .inside textarea {
                width: 40%;
                height: 100px
            }
        </style>
        <!-- my custom value input -->
        <div class="row" style="margin-bottom: 20px;">
            <label for="installment">خرید قسطی :</label>
            <input type="checkbox"
                   value="<?= get_post_meta(get_the_ID(), 'installment', true) ?>" <?PHP checked(1, get_post_meta(get_the_ID(), 'installment', true)) ?>
                   name="installment">
        </div>

        <div class="row" style="margin-bottom: 20px;">
            <label for="installment">درصد سود (فقط عدد) :</label>
            <input type="text"
                   value="<?= get_post_meta(get_the_ID(), 'sood_pro', true) ?>" <?PHP checked(1, get_post_meta(get_the_ID(), 'sood_pro', true)) ?>
                   name="sood_pro">
        </div>


        <div class="row">

            <label for="pish_darsad_pro" style="vertical-align: top">درصد پیش پرداخت</label>
            <textarea name="pish_darsad_pro" id="pish_darsad_pro" style=""><?php
                $abono_prepayment_percent = get_post_meta(get_the_ID(), 'pish_darsad_pro', true);
                $abono_prepayment_percent = is_array($abono_prepayment_percent) ? implode(PHP_EOL, $abono_prepayment_percent) : "";
                echo $abono_prepayment_percent; ?></textarea>
            <br>

        </div>


        <div class="row">

            <label for="pish_month_pro" style="vertical-align: top">چند ماهه :</label>
            <textarea name="pish_month_pro" id="pish_month_pro" style=""><?php
                $abono_prepayment_percent = get_post_meta(get_the_ID(), 'pish_month_pro', true);
                $abono_prepayment_percent = is_array($abono_prepayment_percent) ? implode(PHP_EOL, $abono_prepayment_percent) : "";
                echo $abono_prepayment_percent; ?></textarea>
            <br>

        </div>


        <div class="row">

            <label for="pish_check_pro" style="vertical-align: top">هر ماه چند چک،</label>
            <textarea name="pish_check_pro" id="pish_check_pro" style=""><?php
                $abono_prepayment_percent = get_post_meta(get_the_ID(), 'pish_check_pro', true);
                $abono_prepayment_percent = is_array($abono_prepayment_percent) ? implode(PHP_EOL, $abono_prepayment_percent) : "";
                echo $abono_prepayment_percent; ?></textarea>
            <br>

        </div>


        <?php
    }

    function wpse_save_meta_fields($post_id)
    {

        // verify nonce
        if (!isset($_POST['wpse_our_nonce']) || !wp_verify_nonce($_POST['wpse_our_nonce'], basename(__FILE__)))
            return 'nonce not verified';

        // check autosave
        if (wp_is_post_autosave($post_id))
            return 'autosave';

        //check post revision
        if (wp_is_post_revision($post_id))
            return 'revision';

        // check permissions
        if ('product' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id))
                return 'cannot edit page';
        } elseif (!current_user_can('edit_post', $post_id)) {
            return 'cannot edit post';
        }

        //so our basic checking is done, now we can grab what we've passed from our newly created form

        $installment = $_POST['installment'];

        $pish_darsad = !empty($_POST['pish_darsad_pro']) ? explode(PHP_EOL, str_replace(" ", "", $_POST['pish_darsad_pro'])) : "";

        $pish_month_pro = !empty($_POST['pish_month_pro']) ? explode(PHP_EOL, str_replace(" ", "", $_POST['pish_month_pro'])) : "";
        $pish_check_pro = !empty($_POST['pish_check_pro']) ? explode(PHP_EOL, str_replace(" ", "", $_POST['pish_check_pro'])) : "";

        $sood_pro = $_POST['sood_pro'];

        //simply we have to save the data now
        global $wpdb;
        if (isset($installment)) {
            update_post_meta(get_the_ID(), 'installment', 1);
        } else {
            update_post_meta(get_the_ID(), 'installment', 0);
        }
        update_post_meta(get_the_ID(), 'pish_darsad_pro', $pish_darsad);
        update_post_meta(get_the_ID(), 'pish_month_pro', $pish_month_pro);
        update_post_meta(get_the_ID(), 'pish_check_pro', $pish_check_pro);
        update_post_meta(get_the_ID(), 'sood_pro', $sood_pro);

    }

    public function installment_template_redirect($template)
    {
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $actual_link = preg_replace('/\?.*/', '', $actual_link);

        if (trim(str_replace(get_site_url(), "", $actual_link), '/') == "installment") {

            require_once AB_ADMIN_DIR . 'partials/abono-installment-page.php';
            die();
        }

        return $template;
    }

    public function add_installment_abono_status($order_statuses)
    {
        $new_order_statuses = array();

        // add new order status after processing
        foreach ($order_statuses as $key => $status) {

            $new_order_statuses[$key] = $status;

            if ('wc-processing' === $key) {
                $new_order_statuses['wc-pay_ins'] = 'اقساطی(پیش پرداخت انجام شده)';
                $new_order_statuses['wc-installment'] = 'اقساطی(در انتظار پرداخت پیش پرداخت)';
            }
        }

        return $new_order_statuses;
    }

    public function misha_editable_order_meta_shipping($order)
    {
        $num_check = get_post_meta($order->get_order_number(), 'cheque_num', true);
        $check = 0;
        for ($i = 1; $i <= $num_check; $i++) {
            $check = get_post_meta($order->get_order_number(), 'check' . $i, true);


            ?>
            <div class="address">
                <p<?php if (empty($check)) echo ' class="none_set"' ?>>
                    <strong> تاریخ چک : <?= $i ?></strong>
                    <?php echo (!empty($check)) ? $check : 'Anytime.' ?>
                </p>
            </div>
            <div class="edit_address"><?php
            woocommerce_wp_text_input(array(
                'id' => 'shippingdate' . $i,
                'label' => 'تاریخ چک :' . $i,
                'wrapper_class' => 'form-field-wide',
                'class' => 'selectDate',
                'style' => 'width:100%',
                'value' => $check,
                'description' => 'تاریخ را وارد کنید.'
            ));
            ?></div><?php
        }
        ?>
        <script>
            jQuery(document).ready(function ($) {
                $('.selectDate').persianDatepicker({
                    formatDate: "YYYY-MM-DD"
                });
            });
        </script>
        <?PHP
    }

    function misha_save_shipping_details($ord_id)
    {


        $num_check = get_post_meta($ord_id, 'cheque_num', true);

        for ($i = 1; $i <= $num_check; $i++) {

            update_post_meta($ord_id, 'check' . $i, wc_clean($_POST['shippingdate' . $i]));
            $jalali_date = $_POST['shippingdate' . $i];
            $jalali_date = explode('-', $jalali_date);


            if (function_exists('jdate1')) {
                $miladi = jalali_to_gregorian1($jalali_date[0], $jalali_date[1], $jalali_date[2]);
                $time = mktime(12, 0, 0, $miladi[1], $miladi[2], $miladi[0]);
            }


            update_post_meta($ord_id, 'check' . $i . 'time', wc_clean($time));


        }
    }

    public function msg_email_send_installment_func()
    {
        $username = get_option('abono_sms_username');
        $password = get_option('abono_sms_password');
        $sender = get_option('abono_sms_send_number');
        $matn_sms = get_option('abono_alert_message');

        $nowtime = strtotime("+2 day");
        $lasttime = strtotime("+3 day");
        $searcharr = array();
        $searcharr['relation'] = 'OR';
        $max_check = 10;
        for ($i = 1; $i <= $max_check; $i++) {

            array_push($searcharr, array(
                'key' => 'check' . $i . 'time',
                'value' => array($nowtime, $lasttime),
                'type' => 'numeric',
                'compare' => 'BETWEEN',
            ));

        }

        $args1 = array(
            'post_type' => 'shop_order',
            'post_status' => 'wc-pay_ins',
            'meta_query' => array($searcharr)
        );
        $ids = [];
        $i = 0;

        $query = new WP_Query($args1);


        if ($query->have_posts()) :

            while ($query->have_posts()) :
                $query->the_post();
                $i++;
                //the_title();
                $ids[] = get_the_ID();
            endwhile;
        else:
        endif;
        if (sizeof($ids) > 0) {
            foreach ($ids as $id) {
                $mobile[] = get_post_meta($id, 'mobile', true);
                $fname[] = get_post_meta($id, '_billing_first_name', true);
                $lname[] = get_post_meta($id, '_billing_last_name', true);
            }
        }
        //var_dump($mobile);
        $k = 0;
        $search = array(
            '{fname}',
            '{lname}'
        );

        if (sizeof($mobile) > 0) {
            foreach ($mobile as $mo) {
                $replace = array($fname[$k], $lname[$k]);
                $sms_msg = str_replace($search, $replace, $matn_sms);
                $hi_mobile = $mo;
                $hi_mobile_sender = $sender;
                $hi_username = $username;
                $hi_password = $password;
                require_once(AB_ADMIN_DIR . 'sms.php');
                $sms_obj = new SMS();
//                wp_mail("iranimij@gmail.com", 'gozaresh', $sms_msg);
                $sms_obj->enqueueSample($sms_msg, $hi_mobile, $hi_mobile_sender, $hi_username, $hi_password);

                $k++;
            }
        }
    }

}
