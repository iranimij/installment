<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.iranimij.com
 * @since      1.0.0
 *
 * @package    Abono
 * @subpackage Abono/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Abono
 * @subpackage Abono/public
 * @author     Iman Heydari <iranimij@gmail.com>
 */
class Abono_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @param string $plugin_name The name of the plugin.
     * @param string $version The version of this plugin.
     * @since    1.0.0
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Abono_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Abono_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style("wait-me", plugin_dir_url(__FILE__) . 'css/waitMe.min.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/abono-public.css', array(), $this->version, 'all');


    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Abono_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Abono_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/abono-public.js', array('jquery'), $this->version, true);
        wp_enqueue_script('sweet-alert', plugin_dir_url(__FILE__) . 'js/sweetalert2.all.min.js', array('jquery'), $this->version, true);
        wp_enqueue_script('wait-me', plugin_dir_url(__FILE__) . 'js/waitMe.min.js', array('jquery'), $this->version, true);
        wp_enqueue_script('ajax-functions', plugin_dir_url(__FILE__) . 'js/ajax-functions.js', array('jquery'), $this->version, true);
        wp_localize_script('ajax-functions', 'MyAjax', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'security' => wp_create_nonce('get-calculated-string')
        ));

    }

    public function after_add_to_card_func()
    {
        include_once AB_PUBLIC_DIR . '/partials/after-add-to-cart-tpl.php';
    }

    public function get_calculated_installment_info()
    {
        $result = ["error" => false, "result" => []];
        $variation_id = !empty($_GET['variation_id']) ? $_GET['variation_id'] : false;
        $product_id = !empty($_GET['product_id']) ? $_GET['product_id'] : false;
        $quantity = !empty($_GET['quantity']) ? $_GET['quantity'] : 1;
        $pre_pay_percent = !empty($_GET['pre_pay']) ? $_GET['pre_pay'] : "";
        $installment_num = !empty($_GET['installment_num']) ? $_GET['installment_num'] : "";
        $cheque_number = !empty($_GET['cheque_number']) ? $_GET['cheque_number'] : "";
        $last_price = "";

        $last_price = $this->calculate_last_price($product_id, $quantity, $variation_id);

        $profit = !empty(get_post_meta($product_id, "sood_pro", true)) ? get_post_meta($product_id, "sood_pro", true) : (!empty(get_option("abono_profit_percent")) ? get_option("abono_profit_percent") : 18);
        $input_fields = [
            "pre_pay" => ($last_price * $pre_pay_percent) / 100,
            "installment_num" => intval($installment_num),
            "cheque_number" => intval($cheque_number),
        ];
        $calculated_installment_info = $this->calculate_installment_info($last_price, $profit, $input_fields);
        ob_start();
        include_once AB_PUBLIC_DIR . '/partials/calculated-table.php';

        $calculated_html = ob_get_clean();
        $result = [
            "error" => false,
            "html" => $calculated_html,
        ];

        echo json_encode($result);
        die();
    }

    public function calculate_last_price($product_id, $quantity, $variation_id = null)
    {
        if ($variation_id) {
            $variable_product = new WC_Product_Variation($variation_id);
            $v_sale_price = $variable_product->get_sale_price();
            $last_price = !empty($v_sale_price) ? $v_sale_price : $variable_product->get_price();

        } else {
            $_product = wc_get_product($product_id);
            $sale_price = $_product->get_sale_price();
            $price = $_product->get_price();
            $last_price = !empty($sale_price) ? $sale_price : $price;
        }

        return $last_price * $quantity;
    }

    /**
     * @param $price
     * @param $profit
     * @param $input_fields
     * @return array
     */
    public function calculate_installment_info($price, $profit, $input_fields)
    {
        $p = $price - $input_fields['pre_pay'];
        $r = intval($profit) / 100;
        $f = $input_fields['cheque_number'];
        $bb = ($p * $r * $f) / 12;
        $t = $input_fields['installment_num'] / 12;
        //var_dump($t);die();
        $cc = pow(1 + (($r * $f) / 12), ((12 * $t) / $f));
        $result = ($bb * $cc) / ($cc - 1);
        $total_price = ($result * $input_fields['installment_num'] / $f) + $input_fields['pre_pay'];
        $total_profits = $total_price - $price;
        return [
            "pre_pay_value" => number_format($input_fields['pre_pay']),
            "cheque_month" => intval($input_fields['installment_num']),
            "cheque_value" => number_format(round($result)),
            "cheque_num" => round($input_fields['installment_num'] / $f),
            "profit" => number_format(round($total_profits)),
            "total_price" => number_format(round($total_price))
        ];
    }

    public function before_add_to_card_func()
    {
        include_once AB_PUBLIC_DIR . '/partials/before-add-to-cart-tpl.php';
    }

    function woocommerce_cross_sell_display1()

    {


        require_once AB_ADMIN_DIR . 'partials/after_card_table.php';


    }

    function validate_new_installment_field()
    {
        if (isset($_SESSION['abono_installment'])) {

            if (!is_user_logged_in()) {
                if (!$_POST['createaccount']) {
                    wc_add_notice(__('برای خرید قسطی باید عضو شوید'), 'error');
                }
            }

        }
    }

    function cart_totals_after_order_total()
    {
        $installment = $_SESSION['abono_installment'];
        if (isset($installment)) {
            $variation_id = !empty($installment['variation_id']) ? $installment['variation_id'] : false;
            $product_id = !empty($installment['product_id']) ? $installment['product_id'] : false;
            $quantity = !empty($installment['quantity']) ? $installment['quantity'] : 1;
            $pre_pay_percent = !empty($installment['pre_pay']) ? $installment['pre_pay'] : "";
            $installment_num = !empty($installment['installment_num']) ? $installment['installment_num'] : "";
            $cheque_number = !empty($installment['cheque_number']) ? $installment['cheque_number'] : "";

            $last_price = $this->calculate_last_price($product_id, $quantity, $variation_id);

            $profit = !empty(get_post_meta($product_id, "sood_pro", true)) ? get_post_meta($product_id, "sood_pro", true) : (!empty(get_option("abono_profit_percent")) ? get_option("abono_profit_percent") : 18);
            $input_fields = [
                "pre_pay" => ($last_price * $pre_pay_percent) / 100,
                "installment_num" => intval($installment_num),
                "cheque_number" => intval($cheque_number),
            ];
            $calculated_installment_info = $this->calculate_installment_info($last_price, $profit, $input_fields);

            $installment = $calculated_installment_info;
            $cart = WC()->cart;
            $fees = $cart->tax_total + $cart->shipping_tax_total + $cart->shipping_total + $cart->fee_total;
            $pre_pay = str_replace(",", "", $installment['pre_pay_value']);

            if (is_array($installment)) {
                if (sizeof($installment) > 0) {
                    ?>
                    <tr class="order-paid">
                        <th><?php _e('پیشپرداخت', 'woocommerce-deposits'); ?></th>
                        <td><?php echo number_format($pre_pay + $fees);
                            echo get_woocommerce_currency_symbol(get_woocommerce_currency()); ?></td>
                    </tr>
                    <tr class="order-paid">
                        <th><?php _e('مبلغ هر قسط(چک)', 'woocommerce-deposits'); ?></th>
                        <td><?php echo $installment['cheque_value'];
                            echo get_woocommerce_currency_symbol(get_woocommerce_currency()); ?></td>
                    </tr>
                    <tr class="order-paid">
                        <th><?php _e('تعداد چک', 'woocommerce-deposits'); ?></th>
                        <td><?php echo $installment['cheque_num'];
                            echo ' فقره'; ?></td>
                    </tr>
                    <tr class="order-paid">
                        <th><?php _e('تعداد اقساط', 'woocommerce-deposits'); ?></th>
                        <td><?php echo $installment_num;
                            echo ' ماهه'; ?></td>
                    </tr>

                    <?PHP

                    $total_price = str_replace(",", "", $installment['total_price']);
                    ?>
                    <tr class="order-remaining">
                        <th><?php _e('مبلغ نهایی', 'woocommerce-deposits'); ?></th>
                        <td><?php echo number_format($total_price + $fees);
                            echo get_woocommerce_currency_symbol(get_woocommerce_currency()); ?></td>
                    </tr>
                    <style>
                        .order-total {
                            display: none;
                        }
                    </style>
                <?php }
            }
        }
    }

    function abono_calculated_total($cart_total, $cart)
    {

        if (isset($_SESSION['abono_installment'])) {

            $installment = $_SESSION['abono_installment'];

            $variation_id = !empty($installment['variation_id']) ? $installment['variation_id'] : false;
            $product_id = !empty($installment['product_id']) ? $installment['product_id'] : false;
            $quantity = !empty($installment['quantity']) ? $installment['quantity'] : 1;
            $pre_pay_percent = !empty($installment['pre_pay']) ? $installment['pre_pay'] : "";

            $last_price = $this->calculate_last_price($product_id, $quantity, $variation_id);
            $cart = WC()->cart;
            $fees = $cart->tax_total + $cart->shipping_tax_total + $cart->shipping_total + $cart->fee_total;
            return (($last_price * $pre_pay_percent) / 100) + $fees;

        }else{
            return $cart_total;
        }

    }

    public function getInstallmentInfoifExist()
    {
        $installment = $_SESSION['abono_installment'];
        if (isset($installment)) {
            $variation_id = !empty($installment['variation_id']) ? $installment['variation_id'] : false;
            $product_id = !empty($installment['product_id']) ? $installment['product_id'] : false;
            $quantity = !empty($installment['quantity']) ? $installment['quantity'] : 1;
            $pre_pay_percent = !empty($installment['pre_pay']) ? $installment['pre_pay'] : "";
            $installment_num = !empty($installment['installment_num']) ? $installment['installment_num'] : "";
            $cheque_number = !empty($installment['cheque_number']) ? $installment['cheque_number'] : "";

            $last_price = $this->calculate_last_price($product_id, $quantity, $variation_id);

            $profit = !empty(get_post_meta($product_id, "sood_pro", true)) ? get_post_meta($product_id, "sood_pro", true) : (!empty(get_option("abono_profit_percent")) ? get_option("abono_profit_percent") : 18);
            $input_fields = [
                "pre_pay" => ($last_price * $pre_pay_percent) / 100,
                "installment_num" => intval($installment_num),
                "cheque_number" => intval($cheque_number),
            ];
            $calculated_installment_info = $this->calculate_installment_info($last_price, $profit, $input_fields);
            return isset($calculated_installment_info) ? $calculated_installment_info : null;
        } else {
            return null;
        }
    }
    public function customise_checkout_field($checkout){
        echo '<div id="customise_checkout_field">';
        woocommerce_form_field('mobile', array(
            'type' => 'text',
            'class' => array(
                'my-field-class form-row-wide'
            ),
            'label' => 'موبایل',
            'placeholder' => '',
            'required' => true,
        ), $checkout->get_value('mobile'));
        echo '</div>';
    }
    function customise_checkout_field_update_order_meta($order_id)
    {
        if (!empty($_POST['mobile'])) {
            update_post_meta($order_id, 'mobile', sanitize_text_field($_POST['mobile']));
        }
        $installment = $this->getInstallmentInfoifExist();
        if (sizeof($installment) > 0) {

            if (!empty($installment['cheque_value'])) {
                update_post_meta($order_id, 'cheque_value', sanitize_text_field(str_replace(",", "", $installment['cheque_value'])));
            }

            if (!empty($installment['pre_pay_value'])) {
                update_post_meta($order_id, 'pre_pay_value', sanitize_text_field(str_replace(",", "", $installment['pre_pay_value'])));
            }

            if (!empty($installment['cheque_num'])) {
                update_post_meta($order_id, 'cheque_num', sanitize_text_field($installment['cheque_num']));
            }

            if (!empty($installment['cheque_month'])) {
                update_post_meta($order_id, 'cheque_month', sanitize_text_field($installment['cheque_month']));
            }
            $cart = WC()->cart;
            $fees = $cart->tax_total + $cart->shipping_tax_total + $cart->shipping_total + $cart->fee_total;
            if (!empty($installment['total_price'])) {

                update_post_meta($order_id, 'total', str_replace(",", "", $installment['total_price']) + $fees);
            }

            update_post_meta($order_id, "fees", $fees);

            for ($i = 1; $i <= $installment['cheque_num']; $i++) {


                update_post_meta($order_id, 'check' . $i, sanitize_text_field(""));


            }
        }

    }

    public function order_item_after_shipping($order_id)
    {
        $pre_pay = get_post_meta($order_id, "pre_pay_value", true);
        $cheque_value = get_post_meta($order_id, "cheque_value", true);
        $total_price = get_post_meta($order_id, "total", true);
        $cheque_num = get_post_meta($order_id, "cheque_num", true);
        $cheque_month = get_post_meta($order_id, "cheque_month", true);
        if (isset($pre_pay) && !empty($pre_pay)) {
            ?>
            <style>
                .wc-order-totals tr:last-child {
                    display: none;
                }
            </style>
            <tr>
                <td class="label">پیشپرداخت :</td>
                <td width="1%"></td>
                <td class="total">
                    <?= number_format($pre_pay) . ' ' . get_woocommerce_currency_symbol(get_woocommerce_currency()) ?>
                </td>
            </tr>
            <tr>
                <td class="label">تعداد چک :</td>
                <td width="1%"></td>
                <td class="total">
                    <?= $cheque_num ?>
                </td>
            </tr>
            <tr>
                <td class="label">تعداد اقساط :</td>
                <td width="1%"></td>
                <td class="total">
                    <?= $cheque_month ?>
                    ماهه
                </td>
            </tr>
            <tr>
            <tr>
                <td class="label">مبلغ هر قسط:</td>
                <td width="1%"></td>
                <td class="total">
                    <?= number_format($cheque_value) . ' ' . get_woocommerce_currency_symbol(get_woocommerce_currency()) ?>
                </td>
            </tr>
            <tr>
                <td class="label">قیمت کل خرید اقساطی:</td>
                <td width="1%"></td>
                <td class="total">
                    <?= number_format($total_price) . ' ' . get_woocommerce_currency_symbol(get_woocommerce_currency()) ?>
                </td>
            </tr>
            <?PHP
        }
    }

    public function change_status_in_thankyou_page($order_id)
    {
        $order = wc_get_order($order_id);
        $order_status = $order->get_status();
        if (($order_status == 'processing')) {

            $orderid = $order->get_id();
            $num_check = get_post_meta($orderid, 'cheque_num', true);
            if (intval($num_check)) {
                $order->update_status('wc-pay_ins');
                $this->installment_order_detail_in_thankyou($order_id);
            }

        }elseif ($order_status == "completed"){

            $orderid = $order->get_id();
            $num_check = get_post_meta($orderid, 'cheque_num', true);
            if (intval($num_check)) {
                $order->update_status('wc-pay_ins');
                $this->installment_order_detail_in_thankyou($order_id);

            }
        }elseif ($order_status == 'Pending payment') {

            $orderid = $order->get_id();
            $num_check = get_post_meta($orderid, 'cheque_num', true);
            if (intval($num_check)) {
                $order->update_status('installment');
                $this->installment_order_detail_in_thankyou($order_id);
            }

        }elseif ($order_status == 'on-hold') {

            $orderid = $order->get_id();
            $num_check = get_post_meta($orderid, 'cheque_num', true);
            if (intval($num_check)) {
                $order->update_status('installment');
                $this->installment_order_detail_in_thankyou($order_id);
            }
        }
        $_SESSION['abono_installment'] = null;
    }

    public function installment_order_detail_in_thankyou($orderid)
    {

        $order = wc_get_order($orderid);
        echo '
            <h2>مشخصات خرید اقساطی شما</h2>
            <ul class="woocommerce-order-overview woocommerce-thankyou-order-details order_details">

                <li class="woocommerce-order-overview__order order">
					شماره سفارش
					<strong > ' . $order->get_order_number() . '</strong >
				</li >

                <li class="woocommerce-order-overview__order order">
					تاریخ
					<strong > ' . wc_format_datetime($order->get_date_created()) . '</strong >
				</li >

                <li class="woocommerce-order-overview__payment-method method" >
            مبلغ پیش پرداخت : 					<strong > ' . get_post_meta($orderid, 'pre_pay_value', true) . get_woocommerce_currency_symbol(get_woocommerce_currency()) . '</strong >
				</li >
				<li class="woocommerce-order-overview__payment-method method" >
            مبلغ هر چک : 					<strong > ' . get_post_meta($orderid, 'cheque_value', true) . '</strong >
				</li >

                <li class="woocommerce-order-overview__payment-method method" >
            مبلغ نهایی پرداخت : 					<strong > ' . ceil(get_post_meta($orderid, 'total', true)) . '</strong >
				</li >
                <li class="woocommerce-order-overview__payment-method method" >
            تعداد اقساط : 					<strong > ' . get_post_meta($orderid, 'cheque_num', true) . '</strong >
				</li >
				<li class="woocommerce-order-overview__payment-method method" >
            روش پرداخت : 					<strong > ' . wp_kses_post($order->get_payment_method_title()) . '</strong >
				</li >

            </ul > ';
    }

    public function add_new_statuses(){
        register_post_status('wc-pay_ins', array(
            'label' => 'اقساطی(پیش پرداخت انجام شده)',
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('اقساطی(پیش پرداخت انجام شده) <span class="count">(%s)</span>', 'اقساطی(پیش پرداخت انجام شده) <span class="count">(%s)</span>')
        ));
        register_post_status('wc-installment', array(
            'label' => 'اقساطی (معلق)',
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('اقساطی(معلق) <span class="count">(%s)</span>', 'اقساطی(معلق) <span class="count">(%s)</span>')
        ));
    }

    public function show_order_installment_info($total_rows, $order){
        @$orderid = $order->id;
        $num_check = get_post_meta($orderid, 'cheque_num', true);

        if (intval($num_check)) {


            $total_rows['order_total'] = array(
                'label' => __('مبلغ پیش پرداخت:', 'woocommerce-deposits'),
                'value' => ceil(get_post_meta($orderid, 'pre_pay_value', true)) . get_woocommerce_currency_symbol(get_woocommerce_currency())
            );


            $total_rows['oneghest'] = array(
                'label' => __('مبلغ چک:', 'woocommerce-deposits'),
                'value' => get_post_meta($orderid, 'cheque_value', true) . get_woocommerce_currency_symbol(get_woocommerce_currency())
            );


            $total_rows['num_check'] = array(
                'label' => __('تعداد چک:', 'woocommerce-deposits'),
                'value' => get_post_meta($orderid, 'cheque_num', true)
            );

            $total_rows['total'] = array(
                'label' => 'مبلغ نهایی',
                'value' => ceil(get_post_meta($orderid, 'total', true)) . get_woocommerce_currency_symbol(get_woocommerce_currency())
            );

            for ($i = 1; $i <= $num_check; $i++) {

                $total_rows['check' . $i] = array(
                    'label' => 'تاریخ چک',
                    'value' => get_post_meta($orderid, 'check' . $i, true)
                );

            }


        }

        return $total_rows;
    }
}
