<?PHP
$installment_is_active = get_post_meta(get_the_ID(), 'installment', true);
if ($installment_is_active):
?>
<span class="single_add_to_cart_button button alt get_installment_info disabled" id="calculate_btn">
محاسبه و نمایش اقساط
</span>
<div class="abono_calculated_result installment_parent">

</div>
<?PHP endif; ?>