<?PHP
$product_id = get_the_ID();
$pish_darsad_pro = get_post_meta($product_id, 'pish_darsad_pro', true);
$pish_check_pro = get_post_meta($product_id, 'pish_check_pro', true);
$pish_month_pro = get_post_meta($product_id, 'pish_month_pro', true);
$installment_is_active = get_post_meta($product_id, 'installment', true);

if ($pish_darsad_pro != "") {

    $pish_darsad = $pish_darsad_pro;

} else {
    $pish_darsad = get_option('abono_prepayment_percent');
}

if ($pish_check_pro != "") {
    $pish_check = $pish_check_pro;
} else {
    $pish_check = get_option('abono_check_number');
}

if ($pish_month_pro != "") {
    $pish_month = $pish_month_pro;
} else {
    $pish_month = get_option('abono_month_number');
}
if ($installment_is_active):
?>
<style>
    .installment_field_box {
        padding: 10px;
        display: flex;
        align-items: baseline;
    }

    .installment_field_box label {
        width: 150px;
        display: inline-block;
        font-weight: bold;
    }

    .installment_field_box select {
        width: 150px;
        display: inline-block;
    }
</style>
<div class="installment_parent">

    <div class="installment_field_box">
        <label for="">پیش پرداخت</label>
        <select name="pre_pay" id="pre_pay">
            <?PHP
            if (is_array($pish_darsad) && sizeof($pish_darsad) > 0) {
                ?>
                <option value="">انتخاب کنید</option>
                <?PHP
                foreach ($pish_darsad as $item) {
                    ?>
                    <option value="<?= $item ?>"><?= $item ?> درصد</option>
                    <?PHP

                }
            }
            ?>
        </select>
    </div>

    <div class="installment_field_box">
        <label for="">تعداد اقساط</label>
        <select name="installment_num" id="installment_num">
            <?PHP
            if (is_array($pish_month) && sizeof($pish_month) > 0) {
                ?>
                <option value="">انتخاب کنید</option>
                <?PHP
                foreach ($pish_month as $item) {
                    ?>
                    <option value="<?= $item ?>"><?= $item ?> ماهه</option>
                    <?PHP

                }
            }
            ?>
        </select>
    </div>

    <div class="installment_field_box">
        <label for="">تعداد چک</label>
        <select name="cheque_number" id="cheque_number">
            <?PHP
            if (is_array($pish_check) && sizeof($pish_check) > 0) {
                ?>
                <option value="">انتخاب کنید</option>
                <?PHP
                foreach ($pish_check as $item) {
                    ?>
                    <option value="<?= $item ?>"> هر <?= $item ?> ماه یک چک</option>
                    <?PHP

                }
            }
            ?>
        </select>
    </div>

</div>
<?PHP endif; ?>