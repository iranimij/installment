<table class="table table-condensed" align="center">
    <thead>
    <tr>
        <th colspan="2" class="text-center" style="text-align: center">نتیجه محاسبه</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td class="text-left">پیش پرداخت :</td>
        <td class="text-info"><span
                    class="text-danger pishpay"><?= $calculated_installment_info['pre_pay_value'] ?></span> تومان
        </td>
    </tr>
    <tr>
        <td class="text-left">مبلغ هر چک :</td>
        <td class="text-info"><span
                    class="text-danger every_check"><?= $calculated_installment_info['cheque_value'] ?></span> تومان
        </td>
    </tr>
    <tr>
        <td class="text-left">تعداد چکها :</td>
        <td class="text-info"><span
                    class="text-danger number_check"><?= $calculated_installment_info['cheque_num'] ?></span> فقره
        </td>
    </tr>
    <tr>
        <td class="text-left">سود :</td>
        <td class="text-info"><span class="text-danger sood"><?= $calculated_installment_info['profit'] ?></span> تومان
        </td>
    </tr>
    <tr>
        <td class="text-left">قیمت کل :</td>
        <td class="text-info"><span
                    class="text-danger total_asl"><?= $calculated_installment_info['total_price'] ?></span> تومان
        </td>
    </tr>
    </tbody>
</table>