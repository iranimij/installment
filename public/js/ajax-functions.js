$ = jQuery;
jQuery(document).ready(function ($) {

    $('input.variation_id').change(function () {
        getCalculatedInstallmentInfo();
    });
    $('.get_installment_info').on("click", function () {
        getCalculatedInstallmentInfo();
    });

});

function getCalculatedInstallmentInfo() {
    $('.abono_continiue_installment').removeClass("active");
    jQuery('.abono_calculated_result').css({"opacity": "0.5"});
    if ('' != jQuery('input.variation_id').val()) {
        var var_id = jQuery('input.variation_id').val();
        var product_id = jQuery('button[name="add-to-cart"]').val();
        var quantity = jQuery('input[name="quantity"]').val();
        var pre_pay = jQuery('select[name="pre_pay"]').val();
        var installment_num = jQuery('select[name="installment_num"]').val();
        var cheque_number = jQuery('select[name="cheque_number"]').val();
        if (pre_pay == "" || installment_num == "" || cheque_number == "") {
            $('#calculate_btn').addClass("disabled");
            Swal.fire({
                title: 'خطا !',
                text: 'لطفا تمام گزینه ها را انتخاب کنید.',
                icon: 'error',
                confirmButtonText: 'OK'
            });
        } else {
            if (!isInt(installment_num/cheque_number)){
                Swal.fire({
                    title: 'خطا !',
                    text: 'لطفا تعداد ماه و چک صحیح را انتخاب کنید.',
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
            }else{
            $('.installment_parent').waitMe({
                effect: 'bounce',
                text: '',
                maxSize: '',
                textPos: 'vertical',
                fontSize: '',
                source: '',
            });
            var data = {
                action: 'get_calculated_installment_info',
                security: MyAjax.security,
                variation_id: var_id,
                product_id,
                quantity,
                pre_pay,
                installment_num,
                cheque_number
            };
            jQuery.ajax({
                url: MyAjax.ajaxurl,
                data: data,
                method: "GET",
                dataType: 'json',
                success: function (result) {
                    $('.installment_parent').waitMe("hide");
                    $('.abono_continiue_installment').addClass("active");
                    jQuery('.abono_calculated_result').html(result.html);
                    jQuery('.abono_calculated_result').css({"opacity": "1"});
                }
            });
            }
        }
    }
}

$('.installment_field_box select').on("change", function () {
    var pre_pay = jQuery('select[name="pre_pay"]').val();
    var installment_num = jQuery('select[name="installment_num"]').val();
    var cheque_number = jQuery('select[name="cheque_number"]').val();
    if (pre_pay == "" || installment_num == "" || cheque_number == "") {
        $('#calculate_btn').addClass("disabled");
    }else{
        $('#calculate_btn').removeClass("disabled");
    }
});

function isInt(n) {
    return n % 1 === 0;
}